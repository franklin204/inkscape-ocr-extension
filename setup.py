#!/usr/bin/env python3
# coding=utf-8
#
# OCR Inkscape extension
#
# Copyright (C) 2023 Franklin JARRIER, https://gitlab.com/franklin204/inkscape-extension
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
"""
OCR extension setup.
"""

from setuptools import setup

setup(
        name='inkscape-ocr-extension',
        version='0.0.3',
        description='OCR Text recognition',
        long_description='OCR Text recognition based on Tesseract',
        author='Franklin JARRIER',
        url='https://gitlab.com/franklin204/inkscape-ocr-extension',
        author_email='kml2svg@gmail.com',
        license='AGPLv3',
        install_requires=["pytesseract"],
        setup_requires=["pytest-runner"]
)
