#!/usr/bin/env python3
# coding=utf-8
#
# OCR Inkscape extension
#
# Copyright (C) 2023. Franklin JARRIER, https://gitlab.com/franklin204/inkscape-extension
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

"""
Add OCR
"""

import os
import tempfile
import subprocess
from typing import Iterable
import xml.etree.ElementTree as ET
from inkex.command import inkscape
from inkex import Transform
import inkex


class OCR(inkex.EffectExtension):
    """Extract text from images"""

    NO_TEXT_RESULT = f"\nCannot extract text with current parameters. \n" \
                     f"Try to :\n" \
                     f" - increase resolution (800 or 1200) and remove tesseract extra options.\n" \
                     f" - modify languages\n" \
                     f" - try an other 'Page Segmentation (--ppm)' option\n" \
                     f"\n"

    def add_arguments(self, pars):
        pars.add_argument(
            "-s",
            "--selected_only",
            type=inkex.Boolean,
            help="Extract text from only selected images",
            default=True,
        )
        pars.add_argument(
            "-l",
            "--add_to_new_layer",
            type=inkex.Boolean,
            help="Add to new layer",
            default=True,
        )
        pars.add_argument(
            "-f",
            "--user_font",
            default="Arial"
        )
        pars.add_argument(
            "-r",
            "--user_font_ratio",
            type=float,
            default=1.3
        )
        pars.add_argument(
            "-p",
            "--precision",
            type=float,
            default=400
        )
        pars.add_argument(
            "-c",
            "--user_font_color",
            default="000000"
        )
        pars.add_argument(
            "--keep_layout_maxium",
            type=inkex.Boolean,
            default=True
        )
        pars.add_argument(
            "--tesseract_psm",
            default="99"
        )
        pars.add_argument(
            "--tesseract_extras",
            default=""
        )
        pars.add_argument(
            "--mode",
            default=""
        )
        pars.add_argument(
            "--lang",
            default=""
        )
        pars.add_argument(
            "--lang_1",
            default=""
        )
        pars.add_argument(
            "--lang_2",
            default=""
        )
        pars.add_argument(
            "--lang_3",
            default=""
        )

    def __init__(self):
        super().__init__()
        self.errcount = 0

    def effect(self):
        self.is_tesseract_installed()

        self.errcount = 0

        if self.options.selected_only :
            if self.svg.selection.filter(inkex.Image):
                elems = self.svg.selection.filter(inkex.Image)
            else:
                elems = [elem.xpath(".//svg:image") for elem in self.svg.selection]
                elems = [item for sublist in elems for item in sublist]
        else:
            elems = self.svg.xpath("//svg:image")

        if len(elems) == 0:
            exit("No images in selection")

        if self.options.add_to_new_layer:
            destination_element = self.svg.add(inkex.Layer.new('OCR'))
        else:
            destination_element = None

        index = 0
        with tempfile.TemporaryDirectory() as tmp_dir:
            for __, elem in enumerate(elems):
                # Extract image to temp folder
                image_file_path = self.extract_image(elem, tmp_dir, index)
                index = index + 1

                # run OCR
                hocr_result = self.run_tesseract(image_file_path)

                # Extract text from result
                parser = ET.XMLParser(encoding="utf-8")
                tree = ET.parse(hocr_result, parser=parser)
                hocr_data = tree.getroot()

                for page in hocr_data.findall(".//{*}div[@class='ocr_page']"):
                    self.extract_page(page, elem, destination_element)

    def extract_page(self, page, elem, destination_element):
        page_info_bbox, page_info_baseline = self.get_bbox(page, 1, 1)
        result_contain_text = False

        ratio_h = page_info_bbox[2] / elem.bounding_box().size[0]
        ratio_v = page_info_bbox[3] / elem.bounding_box().size[1]

        if not self.options.add_to_new_layer:
            destination_element = elem.getparent().add(inkex.Group())
            destination_element.transform = Transform(elem.transform @ elem.getparent().transform)

        for carea in page.findall(".//{*}div[@class='ocr_carea']"):

            for paragraph in carea.findall(".//{*}p[@class='ocr_par']"):
                paragraph_tspan = destination_element.add(inkex.TextElement())
                paragraph_tspan.style = {'text-align': 'start',
                                         'vertical-align': 'bottom',
                                         'text-anchor': 'start',
                                         'font-family': str(self.options.user_font),
                                         'fill-opacity': '1.0',
                                         'stroke': 'none',
                                         'font-weight': 'normal',
                                         'font-style': 'normal'
                                         }

                paragraph_tspan.style.set_color(self.options.user_font_color, 'fill')
                rotation_x = None
                rotation_y = None
                orientation = None

                for line in paragraph.findall(".//{*}span[@class='ocr_line']"):
                    line_info_bbox, line_info_baseline = self.get_bbox(line, ratio_h, ratio_v)
                    x = elem.bounding_box().x.minimum
                    y = elem.bounding_box().y.minimum
                    orientation = self.get_orientation(line)

                    if not rotation_x:
                        rotation_x = elem.bounding_box().x.minimum
                        rotation_y = elem.bounding_box().y.minimum

                    line_text = ""
                    font_size = 1
                    for word in line.findall(".//{*}span[@class='ocrx_word']"):
                        result_contain_text = True
                        word_info_bbox, word_info_baseline = self.get_bbox(word, ratio_h, ratio_v)

                        if orientation == -90:
                            pos_x = elem.bounding_box().x.minimum - word_info_bbox[1] - word_info_bbox[3]
                            pos_y = elem.bounding_box().y.minimum + word_info_bbox[0] + line_info_bbox[2]
                            font_size = line_info_bbox[2] * self.options.user_font_ratio
                        elif orientation == -180:
                            pos_x = elem.bounding_box().x.minimum - word_info_bbox[2] - line_info_bbox[0]
                            pos_y = elem.bounding_box().y.minimum - line_info_bbox[1]
                            font_size = line_info_bbox[3] * self.options.user_font_ratio
                        elif orientation == -270:
                            pos_x = elem.bounding_box().x.minimum + word_info_bbox[1]  # - word_info_bbox[3]
                            pos_y = elem.bounding_box().y.minimum - word_info_bbox[0]  # - line_info_bbox[2]
                            font_size = line_info_bbox[2] * self.options.user_font_ratio
                        else:
                            pos_x = elem.bounding_box().x.minimum + word_info_bbox[0]
                            pos_y = elem.bounding_box().y.minimum + line_info_bbox[1] + line_info_bbox[3]
                            font_size = line_info_bbox[3] * self.options.user_font_ratio

                        if self.options.keep_layout_maxium:
                            self.add_tspan(paragraph_tspan,
                                           pos_x,
                                           pos_y,
                                           f"{word.text} ",
                                           {'font-size': str(font_size) + 'px'}
                                           )
                        else:
                            line_text = f"{line_text}{word.text} "

                    if not self.options.keep_layout_maxium:
                        self.add_tspan(paragraph_tspan,
                                       x + line_info_bbox[0],
                                       y + line_info_bbox[1] + line_info_bbox[3],
                                       f"{line_text.strip()}",
                                       {'font-size': str(font_size) + 'px'}
                                       )
                if orientation and rotation_x and rotation_y:
                    paragraph_tspan.transform.add_rotate(orientation, rotation_x, rotation_y)
        if not result_contain_text:
            inkex.errormsg(self.NO_TEXT_RESULT)

    def add_tspan(self, parent, x, y, text, style):
        text_span = parent.add(inkex.Tspan(x=str(x), y=str(y)))
        text_span.text = text
        text_span.style = style

    def extract_image(self, elem, tmp_dir, index):
        """Extract image

        if image is embed, export it to file
        if image is a link, use original file

        Return image_file_path

        """
        image_file_path = os.path.join(tmp_dir, "extracted_image_" + str(index) + ".png")
        kwargs = {'export-filename': image_file_path,
                  'export-dpi': self.options.precision,
                  'export-id': elem.attrib["id"],
                  'export-id-only': True
                  }
        inkscape(self.options.input_file, **kwargs)
        return image_file_path

    def is_tesseract_installed(self):
        try:
            p = subprocess.Popen(["tesseract", "-v"], stdout=subprocess.PIPE)
            version_info, err = p.communicate()
            p.wait()
            is_error = True if not "tesseract v" in f"{version_info}" else False
        except FileNotFoundError:
            is_error = True

        if is_error:
            exit("Cannot detect tesseract, please install it first.\n"
                 "Installation instruction available here :\n"
                 " - https://tesseract-ocr.github.io/tessdoc/Installation.html\n"
                 " - https://github.com/UB-Mannheim/tesseract/wiki (windows installer)\n"
                 "\n\n"
                 "Don't forget to add the installation path to your PATH environment variable.")

    def check_supported_language(self, langs):
        p = subprocess.Popen(["tesseract", "--list-langs"], stdout=subprocess.PIPE)
        out, err = p.communicate()
        p.wait()
        avail_lang = f"{out}".split("\\r\\n")
        errors = ""
        for lang in langs:
            if lang not in avail_lang:
                errors = errors + " " + lang
        if errors:
            exit(f"The following selected language are not installed within Tesserac : {errors}")

    def get_best_psm_option(self, image_file_path):
        """Execute tesseract OSD analysis."""
        if self.options.tesseract_psm != "99":
            return self.options.tesseract_psm

        p = subprocess.Popen(["tesseract", f"--psm 0 {image_file_path} -"], stdout=subprocess.PIPE)
        out, err = p.communicate()
        p.wait()
        diagnostic = f"{out}".split("\\r\\n")
        if "Rotate: 0" in diagnostic:
            return "3"

        # use option 1 to get OSD support
        return "1"

    def run_tesseract(self, image_file_path):
        """Execute tesseract OCR."""
        langs = [x for x in [self.options.lang_1, self.options.lang_2, self.options.lang_3] if x and x != "auto"]
        self.check_supported_language(langs)

        lang_args = "+".join(langs)
        if len(lang_args) > 0:
            lang_args = f"-l {lang_args}"

        psm_option = self.get_best_psm_option(image_file_path)

        command_line = f"tesseract --psm {psm_option} {lang_args} {self.options.tesseract_extras} {image_file_path} {image_file_path} hocr"

        os.system(f"{command_line}")

        if not os.path.exists(f"{image_file_path}.hocr"):
            exit(
                f"{self.NO_TEXT_RESULT}"
                f"\n"
                f"Tesseract command line : {command_line}"
            )
        return f"{image_file_path}.hocr"

    def get_attribute_from_title(self, element, name):
        attributes = element.attrib['title'].split(';')
        result = [attribute for attribute in attributes if name in attribute]
        if not result:
            return None
        else:
            return result[0].replace(name, "").strip()

    def get_orientation(self, element):
        text_angle = self.get_attribute_from_title(element, "textangle")
        if not text_angle:
            return 0
        else:
            return -int(text_angle)

    def get_bbox(self, element, ratio_h, ratio_v):
        """Extract bbox from HOCR tesseract output."""
        # inkex.errormsg(f"attributes= {attributes}")
        bbox_attribute = self.get_attribute_from_title(element, "bbox")
        baseline_attribute = self.get_attribute_from_title(element, "baseline")
        if not bbox_attribute:
            inkex.errormsg(f"cannot extract bbox from {element}")

        if not baseline_attribute:
            baseline = [1, 1]
        else:
            baseline_attribute = baseline_attribute.split(' ')
            baseline = [
                float(baseline_attribute[0]),
                float(baseline_attribute[1])
            ]

        element_bbox = bbox_attribute.split(' ')
        bbox = [
            float(element_bbox[0]),
            float(element_bbox[1]),
            float(element_bbox[2]),
            float(element_bbox[3])
        ]
        # remove the base line to align text
        bbox[2] = bbox[2] - bbox[0] + baseline[0]
        bbox[3] = bbox[3] - bbox[1] + baseline[1]
        # apply ratio
        bbox = [
            bbox[0] / ratio_h,
            bbox[1] / ratio_v,
            bbox[2] / ratio_h,
            bbox[3] / ratio_v
        ]
        return bbox, baseline


if __name__ == "__main__":
    OCR().run()
