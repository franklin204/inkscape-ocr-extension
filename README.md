# OCR inkscape extension

## About

This inkscape extension allow to extract text from image directly into Inkscape.
Work use the excellent Tesseract OCR open source library : https://tesseract-ocr.github.io

## Installation instruction:

- Install Tesseract from https://tesseract-ocr.github.io/tessdoc/Installation.html
  (support windows & unix)

- Add the Tesseract installation folder to your `PATH` environment variable

- Copy extension files to your user extension folder :
  - unix: ~/.config/inkscape/extensions
  - windows: %APPDATA%\inkscape\extensions 
    

## Limitation:

Colors and font are not extracted from image (default values are used).

## Licence:

Copyright (C) 2023 Franklin JARRIER, https://gitlab.com/franklin204/inkscape-ocr-extension

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

See <https://www.gnu.org/licenses/>.